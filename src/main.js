/**
 * @author Erin Amer
 */


let gameState, player, snakeGraphics;
// gameSize = size of stage in px
// gridSize = size of grid representing stage
let gameSize = 500, gridSize = 25;
let gridSquareSize = gameSize/gridSize;
// keep track of how many ticks have passed
let tickCount = 0;

const app = new PIXI.Application({
    width: gameSize,
    height: gameSize,
    antialias: false
});

// append stage to a centered div
document.getElementById("game").appendChild(app.view);

gamestate = {
    gameover: false,
    paused: true,
    food: {x: 0, y: 0},
    score: 0,
    speed: 8
};

player = new Snake(gridSize);

// Setting up keyboard controls
let left = keyboard("ArrowLeft"),
    up = keyboard("ArrowUp"),
    right = keyboard("ArrowRight"),
    down = keyboard("ArrowDown");

left.press = () => { player.changeDirection("left"); };
right.press = () => { player.changeDirection("right"); };
up.press = () => { player.changeDirection("up"); };
down.press = () => { player.changeDirection("down"); };

// Setting up visuals for the player
snakeGraphics = new PIXI.Graphics;
app.stage.addChild(snakeGraphics);
app.ticker.add(delta => gameLoop(delta));


// MAIN GAME LOOP CODE

function gameLoop(delta){
    snakeGraphics.clear();

    // draw the snake
    snakeGraphics.beginFill(0xFFFFFF);
    
    player.parts.forEach(part => {
        //console.log(part);
        snakeGraphics.drawCircle(part.x * gridSquareSize + (gridSquareSize/2),
                                 part.y * gridSquareSize + (gridSquareSize/2),
                                 gridSquareSize/2);
    });
    snakeGraphics.endFill();

    // draw an eye :)
    // TODO: make eye rotate with snake direction
    snakeGraphics.beginFill(0x4000FF);
    let snakeHead = player.parts[0];
    snakeGraphics.drawEllipse(snakeHead.x * gridSquareSize + (gridSquareSize/2),
                              snakeHead.y * gridSquareSize + (gridSquareSize/2),
                              gridSquareSize/8, gridSquareSize/8);
    
    // moving the player
    if ((tickCount+1) % gamestate.speed == 0)
        player.moveSnake();
    
    tickCount++;
}