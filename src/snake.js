/**
 * @author Erin Amer
 * The snake. Assumes a 25x25 grid starting at (0,0)
 */
class Snake {

    /**
     * @member {string} direction - Direction that the snake is currently moving
     */
    direction = "left";

    /**
     * @member {SnakePart[]} parts - Array of each of the snake's parts
     */
    parts;

    /**
     * @member {number} gridSize - How many squares are on the grid
     */
    gridSize;

    /**
     * Initialize the Snake's head's position
     * And add 3 more nodes downward
     * @param {number} gridSize 
     */
    constructor(gridSize) {
        let midpoint = Math.floor(gridSize/2);
        this.gridSize = gridSize;

        this.parts = [
            new SnakePart(midpoint, midpoint-1),
            new SnakePart(midpoint, midpoint),
            new SnakePart(midpoint, midpoint+1),
            new SnakePart(midpoint, midpoint+2)
        ];
    }

    /**
     * Add a part to the tail end
     */
    growSnake() {

    }

    /**
     * Add a part to the head, delete tail
     */
    moveSnake() {
        let newx = this.parts[0].x;
        let newy = this.parts[0].y;

        if (this.direction === "up") {
            // wrap around if negative
            if (newy - 1 < 0)
                newy = this.gridSize - 1;
            else
                newy -= 1;
        } else if (this.direction == "left") {
            if (newx - 1 < 0)
                newx = this.gridSize - 1;
            else
                newx -= 1;
        } else if (this.direction == "down") {
            newy = (newy + 1) % this.gridSize;
        } else if (this.direction == "right") {
            newx = (newx + 1) % this.gridSize;
        }
        this.parts.unshift(new SnakePart(newx, newy));
        this.parts.pop();
    }

    /**
     * Getter function for the snake's head
     * @returns {SnakePart}
     */
    getHead() {
        return this.parts[0];
    }

    /**
     * Check if the head collides with the body
     */
    checkSelfCollision() {

    }

    /**
     * 
     * @param {string} dir - The new direction to switch to
     */
    changeDirection (dir) {
        // don't want to switch to the opposite direction
        if (dir === "left" && this.direction != "right")
            this.direction = "left";
        else if (dir === "right" && this.direction != "left")
            this.direction = "right";
        else if (dir === "up" && this.direction != "down")
            this.direction = "up";
        else if (dir === "down" && this.direction != "up")
            this.direction = "down";
    }   
}

/**
 * @author Erin Amer
 * A part of the snake
 */
class SnakePart {

    /**
     * @member {number} x
     */
    x;

    /**
     * @member {number} y
     */
    y;
    
    /**
     * 
     * @param {number} x 
     * @param {number} y 
     */
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Check if this part collides with the specified object
     * @param {any} obj - Object that has x, y properties
     */
    checkCollision(obj) {

    }
}